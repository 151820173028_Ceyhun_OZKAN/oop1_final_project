/**
* @file RobotAPITest.cpp
* @author Ceyhun �zkan (ceyhunozkan334@gmail.com)  ID:151820173028
* @date January, 2021
* @brief It is written to test whether the sonar robot control class is working correctly.
*/

#if 1

#include "RobotControl.h"
#include "PioneerRobotInterface.h"
#include "Pose.h"
#include "MainMenu.h"
#include <iostream>
using namespace std;

int main() {
	RobotControl* robotAPI = new RobotControl();
	PioneerRobotAPI* robot;
	robot = new PioneerRobotAPI;
	robotAPI->print();

	int accessCode = 0;
	cout << "Enter Access Code : ";
	cin >> accessCode;

	if (robotAPI->openAccess(accessCode))
	{
		robotAPI->print();

		MainMenu monu(robot);

		while (1){
			int _accessCode = 0;
			cout << "Enter Access Code : ";
			cin >> _accessCode;

			if (robotAPI->closeAccess(_accessCode))
			{
				cout << "Access Closed Successfully." << endl;
				break;
			}
			else
				cout << "Access Close cannot be done." << endl;
		}

		system("pause");
		return 0;
	}
	else
	{
		cout << "Wrong Access Code!" << endl;
	}

	delete robot;

	system("pause");
	return 0;
}

#endif