/**
* @file RangeSensor.cpp
* @author Fatih Hevesli 152120181037@ogrenci.ogu.edu.tr
* @author Ceyhun �zkan (ceyhunozkan334@gmail.com)  ID:151820173028
* @date January, 2021
* @brief SonarSensor and LaserSensor classes are grouped under an abstract interface class called RangeSensor. 
* Thus, both sensors can be accessed with the same interface, and our software will not be affected for new sensors to be added. 
* In addition, sensors are associated with RobotInterface via the RangeSensor class. 
* A member function called updateSensor () has been added to the RangeSensor class to update the sensor data with the data from the robot.
*/
#include "RangeSensor.h"
using namespace std;

/*!
* \brief Default constructor method.
*/
RangeSensor::RangeSensor()
{
	this->ranges = NULL;
	this->robotAPI = nullptr;
}
/*!
* \brief Method returns the sensor value given the index. Implements the function similar to getRange (i).
* @param i is int variable.
* @return this->ranges[i].
*/
float RangeSensor::operator[](int i) {
	return this->ranges[i];
}