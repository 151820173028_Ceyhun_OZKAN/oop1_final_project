var indexSectionsWithContent =
{
  0: "abcdefghilmnopqrstuw~",
  1: "cehlmnpqrs",
  2: "celmnpqrs",
  3: "abcdefgilmopqrstuw~",
  4: "nprs",
  5: "cmpqrs",
  6: "d",
  7: "flr",
  8: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends"
};

