﻿#include "RobotControl.h"
#include "Path.h"
#include "Record.h"

/*!
* \brief Constructor Method. It produces from pose and pioneerrobotapi classes.
* @return nothing.
*/
RobotControl::RobotControl()
{
	this->position = new Pose();
	this->robotAPI = new PioneerRobotAPI();
	this->state = 0;
	string name, surname;
	int accessCode;
	cout << "Enter\n Name: ";
	cin >> name;
	cout << " Surname : ";
	cin >> surname;
	cout << " Access Code (4-digits): ";
	cin >> accessCode;
	this->_operator = new RobotOperator(name, surname, accessCode);
	this->accessState = false;
}
/*!
* \brief It makes the robot turn left.
* @return nothing.
*/
void RobotControl::turnLeft(){
	if (this->accessState)
	{
		this->robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
	}
	else
	{
		cout << "Access for this operator did not granted." << endl;
	}
}
/*!
* \brief It makes the robot turn right.
* @return nothing.
*/
void RobotControl::turnRight()
{
	if (this->accessState)
	{
		this->robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
	}
	else
	{
		cout << "Access for this operator did not granted." << endl;
	}
}
/*!
* \brief Allows the robot to move at a given speed (mm / s)
* @return nothing.
*/
void RobotControl::forward(float speed)
{
	if (this->accessState)
	{
		this->robotAPI->moveRobot(speed);
	}
	else
	{
		cout << "Access for this operator did not granted." << endl;
	}
}
/*!
* \brief Allows the robot to go backward at a given speed (mm / s)
* @return nothing.
*/
void RobotControl::backward(float speed)
{
	if (this->accessState)
	{
		this->robotAPI->moveRobot(speed); //If the speed is positive, it can be necessary that is negative.
	}
	else
	{
		cout << "Access for this operator did not granted." << endl;
	}
}
/*!
* \brief The robot rotates its position.
* @return pos.
*/
Pose RobotControl::getPose()
{
	if (this->accessState)
	{
		Pose pos(this->position->getX(), this->position->getY(), this->position->getTh());
		return pos;
	}
	else
	{
		cout << "Access for this operator did not granted." << endl;
	}
}
/*!
* \brief The robot assigns its position. The next moves, the new position relative to that position
* will occur.
* @return nothing.
*/
void RobotControl::setPose(Pose position)
{
	if (this->accessState)
	{
		this->position->setPose(position.getX(), position.getY(), position.getTh());
	}
	else
	{
		cout << "Access for this operator did not granted." << endl;
	}
}
/*!
* \brief It stops the robot from turning.
* @return nothing.
*/
void RobotControl::stopTurn()
{
	if (this->accessState)
	{
		if (this->state == PioneerRobotAPI::DIRECTION::right) {
			this->turnLeft();
		}
		else if (this->state == PioneerRobotAPI::DIRECTION::left) {
			this->turnRight();
		}
		else {
			//forward is forward;
		}
	}
	else
	{
		cout << "Access for this operator did not granted." << endl;
	}
}
/*!
* \brief Operator Robot also rotates position, angle and direction.
* @return nothing.
*/
void RobotControl::print() const
{
	if (this->accessState)
	{
		cout << "Position (x, y): " << this->position->getX() << ", " << this->position->getY() << endl;
		cout << "Angle : " << this->position->getTh() << endl;
		cout << "Direction : " << this->state << endl << endl;
	}
	else
	{
		cout << "Access for this operator did not granted." << endl;
	}
}
/*!
* \brief It stops the robot from moving forward or backward.
* @return nothing.
*/
void RobotControl::stopMove()
{
	if (this->accessState)
	{
		this->robotAPI->stopRobot();
	}
	else
	{
		cout << "Access for this operator did not granted." << endl;
	}
}
/*!
* \brief Method is added to the position (pose) path of the robot.(Using the Path object).
* @return true or false.
*/
bool RobotControl::addToPath() {
	Path* path = new Path();
	Pose pos(this->position->getX(), this->position->getY(), this->position->getTh());

	return path->addPos(&pos);
}
/*!
* \brief With the method, path is cleared. So all added locations are deleted (using the Path object)
* @return true or false.
*/
bool RobotControl::clearPath() {
	Path* path = new Path();
	int x = path->getNumber();
	for (int i = 0; i < x; i++) {
		path->removePos(i);
	}
	if (path->getNumber() == 0)
		return true;
	else
		return false;
}
/*!
* \brief With the method, the locations loaded in the Path object are written to the file. (Using Path and Record objects).
* @return true or false.
*/
bool RobotControl::recordPathToFile() {
	Path* path = new Path();
	Record* record = new Record();
	int x = path->getNumber();
	Pose pos;
	bool hasWritten = false;
	for (int i = 0; i < x; i++) {
		pos = path->getPos(i);
		hasWritten = record->writeLine("x: " + to_string(pos.getX()) + "y: " + to_string(pos.getY()) + "th: " + to_string(pos.getTh()));
	}
	return hasWritten;
}
/*!
* \brief Method is used for access. If the correct password is not entered through this function, no function of the RobotControl class will do anything.
* All functions will return without action when called. If the correct password is given using this function, all member functions will perform the required action 
  (You can define a boolean variable under the RobotControl class and make it true / false depending on whether the password is entered or not, and all functions can control this variable.)
* @param code is int variable.
* @return true or false.
*/
bool RobotControl::openAccess(int code)
{
	bool state = this->_operator->checkAccessCode(code);
	this->accessState = state;
	return state;
}
/*!
* \brief With the method, access will be closed again when the correct password is given. Functions will not operate until they are turned back on.
* @param code is int variable.
* @return true or false.
*/
bool RobotControl::closeAccess(int code)
{
	bool state = this->_operator->checkAccessCode(code);
	this->accessState = !state;
	return state;
}