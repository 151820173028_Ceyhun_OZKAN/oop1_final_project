/**
* @file PioneerRobotInterface.h
* @author Yahya Bekir Canevi (152120181030@ogrenci.ogu.edu.tr)
* @date January, 2021
* @brief Inherited from RobotInterface, this class works directly with PioneerRobotAPI. 
* It also has a connection with the RobotControl class.
*/
#pragma once
#include "RobotInterface.h"
#include "RobotControl.h"
using namespace std;

/*!
* \class PioneerRobotInterface.
* \brief Inherited from RobotInterface, this class works directly with PioneerRobotAPI. 
* It also has a connection with the RobotControl class.
*/
class PioneerRobotInterface : public RobotInterface{
protected:
	PioneerRobotAPI* robotAPI;
	RobotControl* robot;
public:
	PioneerRobotInterface(PioneerRobotAPI* _robotAPI, RobotControl* _robot);
	void turnLeft();
	void turnRight();
	void forward(float speed);
	void print() const;
	void backward(float speed);
	Pose getPose();
	void setPose(Pose position);
	void stopTurn();
	void stopMove();
	void updateSensor(float* ranges);
};