/**
* @file RangeSensor.h
* @author Fatih Hevesli 152120181037@ogrenci.ogu.edu.tr
* @author Ceyhun �zkan (ceyhunozkan334@gmail.com)  ID:151820173028
* @date January, 2021
* @brief SonarSensor and LaserSensor classes are grouped under an abstract interface class called RangeSensor.
* Thus, both sensors can be accessed with the same interface, and our software will not be affected for new sensors to be added.
* In addition, sensors are associated with RobotInterface via the RangeSensor class.
* A member function called updateSensor () has been added to the RangeSensor class to update the sensor data with the data from the robot.
*/
#pragma once
#include "PioneerRobotAPI.h"
#include <iostream>
using namespace std;
/*!
* \class RangeSensor
* \brief It includes ,virtual float getRange,virtual float getAngle,virtual void updateSensor,virtual float getMin,virtual float getMax,virtual float getClosestRange,
*  float operator[].
*/
class RangeSensor {
protected:
	float* ranges;
	PioneerRobotAPI* robotAPI;
public:
	RangeSensor();
	virtual float getRange(int index) const = 0;
	virtual float getAngle(int index) const = 0;
	virtual void updateSensor(float* ranges) = 0;
	virtual float getMin(int& index) = 0;
	virtual float getMax(int& index) = 0;
	virtual float getClosestRange(float startAngle, float endAngle, float& angle) = 0;
	float operator[](int i);
};